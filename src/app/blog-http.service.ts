import { Injectable } from '@angular/core';
import {HttpClient,HttpErrorResponse} from '@angular/common/http';
import'rxjs/add/operator/catch';
import'rxjs/add/operator/do';
import { Observable } from 'rxjs/observable';
@Injectable({
  providedIn: 'root'
})
export class BlogHttpService {

  constructor(public http:HttpClient) {
console.log("httpservice called");


   }


public allBlog;
public currentBlog1;
public eBlog;
public cBlog;
public BaseUrl="https://blogapp.edwisor.com/api/v1/blogs";
public authtoken="N2FlNWI5ODI4ODgzM2Q2YzNiY2NhY2RjZDVkMmU3YTU3Zjg3NTM0ZDdhNjRlOTlkNTJmM2FiYTVjZGU4M2ExYWVmYTQ1MDdhYTE4NTAyMGM5NjdkMjZhMWI2MzdhNWVlMGY5OGNiZTEyNTQxZTcwOTcwYzM4YWM1YzZlNTY3YzUwYg";

public getAllBlogs(): any{

let myresponse=this.http.get(this.BaseUrl + '/all?authToken=' + this.authtoken);
console.log(myresponse);
return myresponse;
}
public getsingleblog(currentBlogId):any{

this.currentBlog1=this.http.get(this.BaseUrl+ '/view/' + currentBlogId + '?authToken='+this.authtoken);
return this.currentBlog1;
}
public editBlog(currentBlogId):any{
this.eBlog=this.http.get(this.BaseUrl+'/'+ currentBlogId +'/edit?authToken='+this.authtoken);
return this.eBlog;
}
public createBlog(data):any{
  this.cBlog=this.http.post(this.BaseUrl+ '/create?authToken='+this.authtoken,data);
  return this.cBlog;
}
}



