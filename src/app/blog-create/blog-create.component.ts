import { Component, OnInit } from '@angular/core';
import { BlogHttpService } from '../blog-http.service';
import { ActivatedRoute, Router } from '@angular/Router';

@Component({
  selector: 'app-blog-create',
  templateUrl: './blog-create.component.html',
  styleUrls: ['./blog-create.component.css']
})
export class BlogCreateComponent implements OnInit {
  
  constructor(public http:BlogHttpService, public route:Router) { }

public blogTitle:string;
public blogDescription:string;
public blogBodyHtml:string;
public blogCategory:string;
public possibleCategories=["comedy","Drama","Action"];





  ngOnInit() {}
  public createBlog(){
    let blogData={
    title:this.blogTitle,
    description:this.blogDescription,
    blogBody:this.blogBodyHtml,
    category:this.blogCategory
    
    }  ;
  this.http.createBlog(blogData).subscribe(

data=>{
   console.log(data);
  setTimeout(() => {
    this.route.navigate(['/blog',data.data.blogId]);
  }, 1000 )
},
error=>{
  console.log("raja lodu error:create")
}

  )
}
  }

