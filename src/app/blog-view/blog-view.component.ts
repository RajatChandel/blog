import { Component, OnInit } from '@angular/core';
import { BlogHttpService } from '../blog-http.service';
import { ActivatedRoute } from '@angular/Router';

@Component({
  selector: 'app-blog-view',
  templateUrl: './blog-view.component.html',
  styleUrls: ['./blog-view.component.css']
})
export class BlogViewComponent implements OnInit {
public currentBlog;
  constructor(public http2:BlogHttpService,private route:ActivatedRoute) { }

  ngOnInit() {
    let myBlogid=this.route.snapshot.paramMap.get('blogId');
    console.log(myBlogid);
this.currentBlog=this.http2.getsingleblog(myBlogid).subscribe(

  
data => {

    this.currentBlog=data["data"];
    
  },
  error=>{
    console.log("raja lodu::error");
  }

)

  }

}
