import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {RouterModule} from '@angular/Router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BlogViewComponent } from './blog-view/blog-view.component';
import { BlogCreateComponent } from './blog-create/blog-create.component';
import { HttpClientModule } from '@angular/common/Http';
import { BlogHttpService } from './blog-http.service';
import{FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BlogViewComponent,
    BlogCreateComponent
   
  ],
  imports: [
    BrowserModule, HttpClientModule,FormsModule,
    RouterModule.forRoot([
   {path:'home', component:HomeComponent},
   {path:'blog/:blogId', component:BlogViewComponent},
   {path:'* *',redirectTo:'home',pathMatch:'full'},
   {path:'create',component:BlogCreateComponent},
    ])



  ],
  providers: [BlogHttpService],
  bootstrap: [AppComponent]
})
export class AppModule { }
